require('dotenv').config();
const express = require('express');
const app = express();
const metrics = require('./utils/metrics');
const responseTime = require('response-time');
var cors = require('cors');
const restResponseTimeHistogram = metrics.restResponseTimeHistogram;
app.use(cors());
app.use(
  responseTime((req, res, time) => {
    if (req.originalUrl) {
      restResponseTimeHistogram.observe(
        {
          method: req.method,
          route: req.originalUrl,
          status_code: res.statusCode,
        },
        time * 1000
      );
    }
  })
);
require('./startup/login').login();
require('./startup/routes')(app);

const port = parseInt(process.env.PORT) || 3000;
const server = app.listen(port, () => {
  console.log(`(updated) Listening on port ${port}...`);
  metrics.startMetricsServer();
});

module.exports = server;
