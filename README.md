# Devops Project
### Features
The objective from this project is to familiarize with the concepts, technologies and tools relating to deployment and Devops. We mainly focused on:

- Create Unit Tests for the api.
- Add push and PR rules to our default branch (master).
- Create a CI/CD pipeline using gitlab-ci with different stages.
- Create unit tests and generate code coverage from the pipeline artifact.
- Create metrics about the application and collect them through Prometheus.
- Visualize metrics on Grafana.
- Create alerts.
- Deploy using docker swarm and portainer.

#### How to run

```sh
$ npm i
```
```sh
$ npm run dev
```

#### How to Test
Run unit test
```sh
$ npm run test
```
Run unit test & generate reports
```sh
$ npm run coverage-pipeline
```
Format the code with prettier
```sh
$ npm run format
```
Analyse code with eslint
```sh
$ npm run lint
```
## Endpoints of the application

| Methods | Endpoints | Description |
| ------- | ------------- | ----------- |
| GET | "/posts" : (day as a query param) | Returns posted products from the producthunt's api | 
| GET | "/metrics" | Displays default & custom metrics |

> PN: API is running on port 3000 & metrics are available on port 9100
### Example
```sh
(GET) => domain:3000/posts?day=2022-1-12 : returns products posted at the mentioned day 
```
```sh
(GET) => domain:9100/metrics : returns the available metrics
```

### Repo Configuration & Rules
- No direct pushes to master => only by PR
- In order to merge, a PR must be approved by at least one reviewer and the pipeline must succeed (owner approval is useless)
- You can check code coverage at the "coverage badge" located in the default branch, while reviewing the code for a PR and at the testing job of the pipeline: regex + cobertura

### For you to test
- API: [http://13.37.212.250:3000](http://13.37.212.250:3000)
- prometheus: [http://13.37.212.250:9090/targets](http://13.37.212.250:9090/targets)
- Grafana: [http://13.37.212.250:3002](http://13.37.212.250:3002/d/ef0FmUJnz/reqs-coutn?tab=query&editPanel=2&viewPanel=2&orgId=1)
- Portainer: [https://13.37.212.250:9443](https://13.37.212.250:9443/#!/2/docker/dashboard) 


