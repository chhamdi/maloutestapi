const express = require('express');
const router = express.Router();
const axios = require('axios');
const auth = require('../middleware/auth');
const loginModule = require('../startup/login');

router.get('/', auth, async (req, res) => {
  try {
    const token = await loginModule.getBearerToken();
    const day = req.query.day;
    const API_POSTS_URL = day
      ? `${process.env.API_URL}/posts?day=${day}`
      : `${process.env.API_URL}/posts`;
    const response = await axios.get(API_POSTS_URL, {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    const { posts } = response.data;
    posts && posts.length > 0
      ? res.send(response.data)
      : res.status(404).send(response.data);
  } catch (err) {
    res.status(500).send({ posts: null });
  }
});

module.exports = router;
