const chai = require('chai');
const chaiHttp = require('chai-http');
chai.should();
const server = require('../index');

chai.use(chaiHttp);
describe('posts requests', () => {
  it('should return posts', (done) => {
    chai
      .request(server)
      .get('/posts?day=2021-06-20')
      .end((err, res) => {
        res.should.have.status(200);
        done();
      });
  });
  it('should return not found', (done) => {
    chai
      .request(server)
      .get('/posts?day=2025-06-20')
      .end((err, res) => {
        res.should.have.status(404);
        done();
      });
  });
});
