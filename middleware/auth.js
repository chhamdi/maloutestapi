const loginModule = require('../startup/login');

module.exports = async (req, res, next) => {
  const token = await loginModule.getBearerToken();
  if (!token) return res.status(401).send('Access denied. No token provided.');
  next();
};
