const axios = require('axios');

const token = {};

const APIConfig = {
  client_id: process.env.CLIENT_ID,
  client_secret: process.env.CLIENT_SECRET,
  grant_type: process.env.GRANT_TYPE,
};

const login = async () => {
  try {
    const response = await axios.post(
      `${process.env.API_URL}/oauth/token`,
      APIConfig
    );
    token['value'] = response.data.access_token;
    return token['value'];
  } catch (err) {
    console.error(err);
  }
};

const getBearerToken = async () => {
  token['value'] = token['value'] ? token['value'] : await login();
  return token['value'];
};

module.exports = {
  login,
  getBearerToken,
};
