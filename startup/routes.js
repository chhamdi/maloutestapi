const express = require('express');
const posts = require('../routes/posts');

module.exports = (app) => {
  app.use(express.json());
  app.use('/posts', posts);
};
